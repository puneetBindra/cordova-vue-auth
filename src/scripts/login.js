export default {
  data () {
    return {
      form: {
        email: '',
        password: ''
      },
      show: true
    }
  },
  methods: {
    onSubmit (evt) {
      evt.preventDefault()
      this.$http.post('https://192.168.1.208:50114/auth/mac/new', {
        'payload': {
          'path*': '/auth/signup/email/'
        }
      }).then(data => {
        alert(data.body.payload.macToken)
      }, function (error) {
        alert(JSON.stringify(error))
      })
    }
  }
}
