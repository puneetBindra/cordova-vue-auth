import {
  Container,
  Row,
  Column,
  Navbar,
  NavbarItem,
  NavbarNav,
  NavbarCollapse,
  ViewWrapper,
  MdMask,
  Btn,
  Card,
  CardBody,
  MdInput,
  Fa
} from 'mdbvue'

import qs from 'qs'

export default {
  name: 'ClassicFormPage',
  components: {
    Container,
    Row,
    Column,
    Navbar,
    NavbarItem,
    NavbarNav,
    NavbarCollapse,
    ViewWrapper,
    MdMask,
    Btn,
    Card,
    CardBody,
    MdInput,
    Fa
  },
  data () {
    return {
      active: {
        0: false
      }
    }
  },
  methods: {
    toggleDropdown (index) {
      for (let i = 0; i < Object.keys(this.active).length; i++) {
        if (index !== i) {
          this.active[i] = false
        }
      }
      this.active[index] = !this.active[index]
    },
    allDropdownsClose (target) {
      for (let i = 0; i < Object.keys(this.active).length; i++) {
        this.active[i] = false
      }
    },
    onClick (e) {
      let parent = e.target
      let body = document.getElementsByTagName('body')[0]
      while (parent !== body) {
        if (parent.classList.contains('dropdown') || parent.classList.contains('btn-group')) {
          return
        }
        parent = parent.parentNode
      }
      this.allDropdownsClose(e.target)
    },
    handleSubmit () {
      this.axios.post('https://vm8:50114/auth/mac/new', qs.stringify({
        'payload': {
          'path*': '/auth/signup/email/'
        }
      })).then(response => {
        const auth = {
          headers: {
            'Authorization': response.data.payload.macToken
          }
        }
        this.axios.post('https://vm8:50114/auth/signup/email', qs.stringify({
          'payload': {
            'email*': 'puneet9.bindra@gmail.com',
            'password*': '12345'
          }
        }), auth).then(response1 => {
          alert(response1)
        }, function (error) {
          console.log(JSON.stringify(error))
        })
      }, function (error) {
        console.log(JSON.stringify(error))
      })
    }
  },
  mounted () {
    document.addEventListener('click', this.onClick)
  },
  destroyed () {
    document.removeEventListener('click', this.onClick)
  }
}
